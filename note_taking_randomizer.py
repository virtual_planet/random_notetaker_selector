import time
import random

team = ['alka', 'george', 'onur', 'mike', 'nicholas', 'nico', 'darran', 'pete', 'mahesh', 'richard', 'vishal', 'edward', 'lucian', 'andreas']
last_user = ""

def last_weeks_user():
    answer = input('Who was last weeks user? If no one took notes, please type "na": ')
    last_user = answer.lower()
    if last_user == 'na':
        return
    elif last_user in team:
        team.remove(last_user)
    elif answer.lower() not in team:
        print('Invalid entry, try again.')
        last_weeks_user()

def lucky_winner():
    winner = random.choice(team)
    print('\n3...')
    time.sleep(1)
    print('2...')
    time.sleep(1)
    print('1...\n')
    time.sleep(1)
    print(f'The lucky winner is...{winner.title()}!')

last_weeks_user()
lucky_winner()
